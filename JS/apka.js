const coordinates = [ {lat: 51.20153079560536, lng: 22.714943889219263},
{lat: 51.205564122639196, lng: 22.71983623800597},
{lat: 51.207607540200776, lng: 22.724814417473155},
{lat: 51.21142526164663, lng: 22.725586893597367},
{lat: 51.21481255228768, lng: 22.727818491289554},
{lat: 51.21497384562659, lng: 22.730650903745012},
{lat: 51.21578030384567, lng: 22.731337549188765},
{lat: 51.21771574593856, lng: 22.727646829928613},
{lat: 51.22018869243189, lng: 22.72344112658565},
{lat: 51.22002741736203, lng: 22.727389337887207},
{lat: 51.22185516840274, lng: 22.723698618627058},
{lat: 51.22674673325798, lng: 22.723870279987995},
{lat: 51.22749923555656, lng: 22.7227544811419},
{lat: 51.22674673325798, lng: 22.71777630167472},
{lat: 51.22895045543677, lng: 22.724900247985456},
{lat: 51.23153028855258, lng: 22.72893428996748},
{lat: 51.235453507511544, lng: 22.725586893429202},
{lat: 51.23325009694625, lng: 22.71580219585578},
{lat: 51.23335758283658, lng: 22.709794048222978},
{lat: 51.23265892006124, lng: 22.705159191477673},
{lat: 51.232766407332534, lng: 22.703700069909708},
{lat: 51.23024039007363, lng: 22.699580197247215},
{lat: 51.226477979648585, lng: 22.69717693819409},
{lat: 51.239322656140224, lng: 22.68207073723043},
{lat: 51.24082723714888, lng: 22.672457701017947},
{lat: 51.2341637190733, lng: 22.6736593305445},
{lat: 51.2267467335141, lng: 22.668681151077323},
{lat: 51.23276640739528, lng: 22.6736593305445},
{lat: 51.24233176894668, lng: 22.6528883058711},
{lat: 51.25178769916944, lng: 22.645335205989863},
{lat: 51.240719768708836, lng: 22.629542360783642},
{lat: 51.23072410630321, lng: 22.651000030900793},
{lat: 51.231584033600626, lng: 22.64928341729142},
{lat: 51.22846672068869, lng: 22.65666485581172},
{lat: 51.23104658092295, lng: 22.669539457882014},
{lat: 51.218468396146655, lng: 22.648596771847668},
{lat: 51.23029413662822, lng: 22.6408720106055},
{lat: 51.219113514883205, lng: 22.646021851433613},
{lat: 51.217608223768536, lng: 22.64739514232111},
{lat: 51.21567277715544, lng: 22.640357026522683},
{lat: 51.188496463848665, lng: 22.661209206409946},
{lat: 51.211694103596855, lng: 22.63932705835706},
{lat:51.187420546001036, lng: 22.671508888066178},
{lat: 51.205349021036014, lng: 22.64361859238049},
{lat: 51.20577922375029, lng: 22.65168667634454},
{lat: 51.20190725466147, lng: 22.664732939775767},
{lat: 51.19771225417315, lng: 22.676234250958565},
{lat: 51.19104249230235, lng: 22.711768152656575},
{lat: 51.19642140793799, lng: 22.67915249409449},
{lat: 51.185268634805496, lng: 22.707729435400104},
{lat: 51.18892682395475, lng: 22.687130071911465},
{lat: 51.2023912683385, lng: 22.687649732053476},
{lat: 51.202713941470556, lng: 22.68258572190583},
{lat: 51.18763573158, lng: 22.699146367177068},
{lat: 51.18838887304781, lng: 22.722492312440707},
{lat: 51.19669033696029, lng: 22.697777752348774},
{lat: 51.1914728286326, lng: 22.687563900764424},
{lat: 51.19975601972809, lng: 22.671427732836328},
{lat: 51.195059018803185, lng: 22.71854410096297},
{lat: 51.20190725431034, lng: 22.662158019345718},
{lat: 51.19534567476309, lng: 22.67949581680038},
{lat: 51.18727687798641, lng: 22.724471093365928}
];

const markerList = [], infowindowList = [];
const selectedMode = document.getElementById("mode");
const selectedTrafficModel = document.getElementById("tmodel");
const dateControl = document.getElementById("departuretime");
const startSelect = document.getElementById("start");
const endSelect = document.getElementById("end");

//dateControl.defaultValue = "2020-01-01T00:00:00";

addOptionsToSelect(startSelect);
addOptionsToSelect(endSelect);

function computeTotalDistance(result) {
  let total = 0;
  const myroute = result.routes[0];

  for (let i = 0; i < myroute.legs.length; i++) {
    total += myroute.legs[i].distance.value;
  }
  total = total / 1000;
  document.getElementById("total").innerHTML = total + " km";
}

function addOptionsToSelect(item){
	for (let i = 0; i < coordinates.length; i++) {
								let option = document.createElement("option");
                option.value = i;
                option.text = "Point: " + (i+1);
                item.add(option, item[i]);
   }
}

function ShowLatLngMouseCursor (){
  const coordsDiv = document.getElementById("coords");
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(coordsDiv);
  map.addListener("mousemove", (event) => {
    coordsDiv.textContent =
      "lat: " +
      Math.round(event.latLng.lat()) +
      ", " +
      "lng: " +
      Math.round(event.latLng.lng());
  });
}

function selectMarkerAndOption(selectElement, optionNum){
	let option;

for (var i=0; i<selectElement.options.length; i++) {
  option = selectElement.options[i];

  if (option.value == optionNum) {
     option.setAttribute('selected', true);
     return; 
  } 
}
}

function createMarkers(mapsy){
	for (let i = 0; i < coordinates.length; i++) {
		if ((i%5 == 0) || (i%7 == 0)){
        const marker = new google.maps.Marker({
    			position: coordinates[i],
    			map: mapsy,
  			});  			
  		markerList.push(marker);
}
}
}

function addOinfowindowMarker(mapsy){
	for (let i = 0; i < markerList.length; i++) {
    const infowindow = new google.maps.InfoWindow({
    content: "<p>Marker Location:" + markerList[i].getPosition() + "</p>",
  });
    infowindowList.push(infowindow);
  }
 }
 
function addListeneeMarker(newMarker, newInfowindow, mapsy){
		google.maps.event.addListener(newMarker, "click ", () => {
    newInfowindow.open(mapsy, newMarker);
  });
}

 function functionForMarker(i) {
    return function() {
        console.log("you clicked region number " + i);
    };
 }
 
//=============================================//

function initMap() {
  const directionsRenderer = new google.maps.DirectionsRenderer();
  const directionsService = new google.maps.DirectionsService();
  
  const map = new google.maps.Map(document.getElementById("map"),
    {
    zoom: 14,
    center: { lat: 51.21686473078004, lng: 22.691507325813518 },
  	//mapTypeControl: false,
    }
  );
  
  
  
  
  //for (let i = 0; i < markerList.length; i++) {
  //	addListeneeMarker(markerList[i].pin[0], markerList[i].pin[1], map);
  //}

  showBikeLayer(map);
  showTrafficLayer(map);
  showTransitLayer(map);
  
  
  directionsRenderer.setMap(map);

  document.getElementById("submit").addEventListener("click", () => {
    calculateAndDisplayRoute(directionsService, directionsRenderer);
  });
}
  
//=============================================//

function showBikeLayer(mapObj){
  const bikeLayer = new google.maps.BicyclingLayer();
  bikeLayer.setMap(mapObj);
}
  function showTrafficLayer(mapObj){
  const trafficLayer = new google.maps.TrafficLayer();
  trafficLayer.setMap(mapObj);
}
  function showTransitLayer(mapObj){
  const transitLayer = new google.maps.TransitLayer();
  transitLayer.setMap(mapObj);
}

function getTimeForm(){
	let timeNew = dateControl.value;
	return new Date(timeNew);
}

function getTimeMillisecondsForm(){
	return getTimeForm().valueOf();
}
//=============================================//
function calculateAndDisplayRoute(directionsService, directionsRenderer) {
  
 let coordinatesStart = coordinates[startSelect.value];
 let coordinatesEnd = coordinates[endSelect.value];
 let deparTimeMseconds = getTimeMillisecondsForm();
 let deparTime = getTimeForm();

 
 const waypts = [];
 const checkboxArray = document.getElementById("waypoints");

  for (let i = 0; i < checkboxArray.length; i++) {
    if (checkboxArray.options[i].selected) {
      waypts.push({
        location: checkboxArray[i].value,
        stopover: document.getElementById("stopover").checked,
      });
    }
  }
 
  directionsService.route(
    {
   
      origin: coordinatesStart,
      destination: coordinatesEnd,
      waypoints: waypts,

      optimizeWaypoints: true,
  		unitSystem: google.maps.UnitSystem.METRIC,
      travelMode: google.maps.TravelMode[selectedMode.value],
      drivingOptions: {
    			departureTime: deparTime,		
          trafficModel: google.maps.TrafficModel[selectedTrafficModel.value]
  		}
    },
    (response, status) => {
      if (status === "OK") {
        directionsRenderer.setDirections(response);
        const route = response.routes[0];
        const summaryPanel = document.getElementById("directions-panel");
        summaryPanel.innerHTML = "";

        // For each route, display summary information.
        for (let i = 0; i < route.legs.length; i++) {
          const routeSegment = i + 1;
          summaryPanel.innerHTML +=
            "<b>Etap trasy: " + routeSegment + "</b><br>";
          summaryPanel.innerHTML += route.legs[i].start_address + " to ";
          summaryPanel.innerHTML += route.legs[i].end_address + "<br>";
          summaryPanel.innerHTML += route.legs[i].distance.text + "<br>";
          summaryPanel.innerHTML += route.legs[i].duration.text + "<br><br>";
        }
      } else {
        window.alert("Directions request failed due to " + status);
      }
    }
  );
}

